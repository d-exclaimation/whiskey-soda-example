//
//  SubscriptionService.scala
//  hello-world
//
//  Created by d-exclaimation on 9:48 AM.
//

import Main.system
import akka.http.scaladsl.server.Route
import io.github.dexclaimation.whiskey.Distillery
import io.github.dexclaimation.whiskey.blend.Blend
import io.github.dexclaimation.whiskey.info.RequestedInfo.RequestedInfo
import io.github.dexclaimation.whiskey.info.ServiceInfo
import io.github.dexclaimation.whiskey.protocol.GraphQLOverWebSocket.SubscriptionTransportWs

import scala.concurrent.ExecutionContext
import scala.util.{Failure, Success}

object SubscriptionService {
  implicit val exe: ExecutionContext = system.executionContext

  val blend = Blend(
    schema = Schema.t,
    userContext = new Ctx(Counter.shared)
  )

  val distillery = new Distillery(res = blend, protocol = SubscriptionTransportWs, log = true)


  def publish[T](query: String, value: T): Unit = {
    distillery.publish[T](query, value)
  }

  def info(enum: RequestedInfo) = distillery
    .info(enum)
    .onComplete {
      case Failure(_) => ()
      case Success(value) => value match {
        case ServiceInfo.QueriesInfo(topics) =>
          println(topics.mkString(", "))
        case ServiceInfo.ClientsInfo(clients) =>
          println(clients.mkString(", "))
        case ServiceInfo.OperationsInfo(map) =>
          println(map.map(x => s"${x._1} -> ${x._2}").mkString(", "))
        case ServiceInfo.AllInfo(clients, ops, topics) =>
          println(topics.mkString(", "))
          println(clients.mkString(", "))
          println(ops.map { case (x, y) => s"$x -> $y" }.mkString(", "))
      }
    }

  def handleWebsocket(ctx: Ctx): Route = distillery.handleWebsocket(ctx)
}
