//
//  Schema.scala
//  hello-world
//
//  Created by d-exclaimation on 9:43 AM.
//

import io.github.dexclaimation.soda.schema.{SodaMutation, SodaQuery, SodaSubscription}
import io.github.dexclaimation.soda.utils.SchemaDefinition.makeSchema
import io.github.dexclaimation.whiskey.tunnel.DataTunnel
import sangria.schema.{BooleanType, IntType}


object Schema {
  val t = makeSchema(Query.t, Mutation.t, Subscription.t)
}

object Query extends SodaQuery[Ctx, DataTunnel] {
  def definition: Def = { t =>
    t.field("currentState", IntType) { c =>
      c.ctx.state()
    }
  }
}

object Mutation extends SodaMutation[Ctx, DataTunnel] {
  def definition: Def = { t =>
    t.field("increment", IntType) { c =>
      val res = c.ctx.incremented()
      SubscriptionService.publish[Int]("counter", res)
      res
    }

    t.field("reset", BooleanType) { c =>
      val res = c.ctx.reset()
      if (res) {
        SubscriptionService.publish[Int]("counter", 0)
      }
      res
    }
  }
}

object Subscription extends SodaSubscription[Ctx, DataTunnel] {
  def definition: Def = { t =>
    t.field("counter", IntType) { c =>
      c.ctx.token.foreach { token =>
        println(s"A subscription by $token")
      }
      c.value.push[Int]
    }
  }
}