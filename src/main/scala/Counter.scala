//
//  Ctx.scala
//  hello-world
//
//  Created by d-exclaimation on 9:42 AM.
//

import java.util.concurrent.atomic.AtomicInteger

class Counter {
  private val count = new AtomicInteger()

  def state: Int = count.intValue()

  def incremented(): Int = count.incrementAndGet()

  def reset(): Boolean = {
    count.compareAndSet(0, 0)
  }
}

object Counter {
  val shared = new Counter()
}