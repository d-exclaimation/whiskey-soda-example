//
//  Ctx.scala
//  hello-world
//
//  Created by d-exclaimation on 12:15 AM.
//

class Ctx(
  private val counter: Counter,
  val token: Option[String] = None
) {
  def incremented() = counter.incremented()
  def reset() = counter.reset()
  def state() = counter.state
}
