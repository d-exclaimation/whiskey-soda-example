import akka.actor.typed.{ActorSystem, SpawnProtocol}
import akka.http.scaladsl.Http
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import akka.http.scaladsl.model.StatusCodes.{BadRequest, InternalServerError, OK}
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import io.github.dexclaimation.whiskey.Distillery
import io.github.dexclaimation.whiskey.tunnel.DataTunnel
import io.github.dexclaimation.whiskey.utils.GraphQLRequestParser
import sangria.ast.Document
import sangria.execution.{ErrorWithResolver, Executor, QueryAnalysisError}
import sangria.marshalling.sprayJson._
import sangria.parser.QueryParser
import spray.json.{JsObject, JsString, JsValue}

import scala.concurrent.ExecutionContextExecutor
import scala.util.{Failure, Success}

object Main extends SprayJsonSupport {
  implicit val system: ActorSystem[SpawnProtocol.Command] = ActorSystem(Distillery.behavior, "ok")

  implicit val executionContext: ExecutionContextExecutor = system.executionContext

  val route: Route = {
    (post & path("graphql") & entity(as[JsValue])) { req =>
      graphQLEndpoint(req)
    } ~ (path("graphql" / "websocket") & optionalHeaderValueByName("Origin")) { auth =>
      val ctx = new Ctx(Counter.shared, auth)
      SubscriptionService.handleWebsocket(ctx)
    } ~ path(Remaining) { _ =>
      getFromResource("assets/playground.html")
    }
  }

  def main(args: Array[String]): Unit = {
    Http()
      .newServerAt("localhost", 4000)
      .bind(route)
  }

  private def graphQLEndpoint(requestJson: JsValue): Route = {
    val JsObject(fields) = requestJson

    val JsString(query) = fields("query")

    val operation = GraphQLRequestParser.getOperationName(fields)

    val vars = GraphQLRequestParser.getVariables(fields)

    QueryParser.parse(query) match {

      // query parsed successfully, time to execute it!
      case Success(queryAst) =>
        complete(executeGraphQLQuery(queryAst, operation, vars))

      // can't parse GraphQL query, return error
      case Failure(error) =>
        complete(BadRequest, JsObject("error" -> JsString(error.getMessage)))
    }
  }

  private def executeGraphQLQuery(query: Document, op: Option[String], vars: JsObject) =
    Executor.execute[Ctx, DataTunnel, JsObject](Schema.t, query, new Ctx(Counter.shared), root = DataTunnel.empty,
      variables = vars,
      operationName = op
    )
      .map(OK -> _)
      .recover {
        case error: QueryAnalysisError => BadRequest -> error.resolveError
        case error: ErrorWithResolver => InternalServerError -> error.resolveError
      }
}
